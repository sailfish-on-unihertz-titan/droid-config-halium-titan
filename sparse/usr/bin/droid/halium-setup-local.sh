    cd /android
    mount tmpfs -t tmpfs dev

    mkdir -p /dev/__properties__ dev/__properties__
    mount -o bind /dev/__properties__ dev/__properties__

    mkdir -p /dev/socket dev/socket
    mount -o bind /dev/socket dev/socket

    mkdir dev/pts
    mount -o bind /dev/pts dev/pts
    chmod 0666 dev/pts/ptmx
    ln -s pts/ptmx dev/ptmx

    cp -a /dev/null dev/null

    mount proc -t proc proc
    mount sys -t sysfs sys
    mount /mnt -o rbind mnt

    if [ ! -d /dev/cpuctl ] && [ -d /sys/fs/cgroup/cpu ]; then
        ln -s /sys/fs/cgroup/cpu /dev/cpuctl
    fi

cp /var/lib/lxc/android/titanconfig /var/lib/lxc/android/config
